import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * <p>
 * Main class which starts the program and manage the arguments
 */
public class clsMain {

    public static void main(String[] args) throws IOException, GeneralSecurityException, ParseException {

        clsGoogleCalendarConnector clsGoogleCalendarConnector = new clsGoogleCalendarConnector();
        Calendar service = clsGoogleCalendarConnector.getCalendarService(); //Starts the connection to the Google Calendar API. The created service will be uses later

        clsGetEvents clsGetEvents = new clsGetEvents();
        clsComputeIcal clsComputeIcal = new clsComputeIcal();

        List<Event> items;
        if (args.length == 4) { //Check if there is a search string
            items = clsGetEvents.getEvents(service, args[0], args[1], args[2]);
            clsComputeIcal.addItemsToIcsCalendar(items, args[3]); //Computes the ics file
        } else if (args.length == 3) {
            items = clsGetEvents.getEvents(service, args[0], args[1]);
            clsComputeIcal.addItemsToIcsCalendar(items, args[2]); //Computes the ics file
        } else {
            System.out.println("Wrong parameters");
            System.exit(1);
        }
    }
}
