import com.google.api.services.calendar.Calendar;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * <p>
 * Main class which initially starts the program and creates the requirement for later use
 */
public class clsMain {

    public static void main(String[] args) throws IOException, GeneralSecurityException, ParseException {

        clsGoogleCalendarConnector clsGoogleCalendarConnector = new clsGoogleCalendarConnector();
        Calendar service = clsGoogleCalendarConnector.getCalendarService(); //Starts the connection to the Google Calendar API. The created service will be uses later

        clsGetEvents clsGetEvents = new clsGetEvents();
        clsGetEvents.setService(service); //Initially sets the service from the Google Calendar API for later use

        clsGUIManager clsGUIManager = new clsGUIManager();
        clsGUIManager.main(args); //Starts the GUI

    }

}
