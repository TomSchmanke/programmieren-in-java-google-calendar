import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 */
public class clsGetEvents {

    private static Calendar service;

    /**
     * Reads out the information from the Google Calendar API and filters them
     * @param service The calendar service from the Google Calendar API
     */
    public void setService(Calendar service) {
        clsGetEvents.service = service;
    }

    /**
     * Reads out the information from the Google Calendar API and filters them
     * @param startDate The lower bound of the date
     * @param endDate   The higher bound of the date
     * @param search    The Keyword which have to be contained in the event title or have to start with when the search string contains a *
     * @return A list of events from the google Calendar
     * @throws ParseException See stringToDate
     * @throws IOException    Can occur when reading the list
     */
    public List<Event> getEvents(String startDate, String endDate, String search) throws ParseException, IOException {

        Events events = service.events().list("primary")
                .setTimeMin(stringToDate(startDate)) //Start
                .setTimeMax(stringToDate(endDate)) //End
                .setOrderBy("startTime") //Sort
                .setSingleEvents(true)
                .execute(); //Request the list of events at the Google Calendar API
        List<Event> googleEvents = events.getItems();

        if (search != null) { //Controls if the input is null
            if (search.endsWith("*")) { //Controls if the input ends with a *
                googleEvents.removeIf(googleEvent -> !googleEvent.getSummary().startsWith(search.substring(0, search.length() - 1))); //Removes all events from the list which doesn't start with the search string
            } else {
                googleEvents.removeIf(item -> !item.getSummary().contains(search)); //Removes all events from the list which doesn't contain the search string
            }
        }

        return googleEvents;

    }

    /**
     * Parses a string with the format dd/MM/yyyy to a Date object. If the input is null it will return null
     * @param strDate The String
     * @return A Date Object
     * @throws ParseException Exception occurs when the String has not the right format
     */
    private DateTime stringToDate(String strDate) throws ParseException {
        if (strDate == null) { //Controls if the input is null
            return null;
        }
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(strDate); //Creates a date object out of the string
        return new DateTime(date);
    }

}
