import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 */
public class clsGUIManager extends Application {

    private static List<Event> events;

    private static TableView<Event> table;
    private static Button settingsButton;
    private static Button aboutButton;
    private static GridPane primaryGridPane;

    private static Label authorLabel;
    private static Button returnButtonAbout;

    private static Button settingsSubmitButton;
    private static Button cancelButtonSettings;
    private static TextField calendarURITextfield;
    private static TextField calendarExportTextfield;
    private static TextField startDateTextfield;
    private static TextField endDateTextfield;
    private static TextField searchTextfield;
    private static GridPane settingsGridPane;
    private static Boolean[] validationArray;

    private static Stage stage;

    private static Image returnImage;
    private static Image aboutusImage;
    private static Image checkmarkImage;
    private static Image exitImage;
    private static Image settingsImage;
    private static Image downloadImage;

    private final clsUserPreferences clsUserPreferences = new clsUserPreferences();
    private final clsComputeIcal clsComputeIcal = new clsComputeIcal();
    private final clsGetEvents clsGetEvents = new clsGetEvents();


    /**
     * Main Method
     * @param args arguments
     * @throws IOException    See clsGetEvents.getEvents
     * @throws ParseException See clsGetEvents.getEvents
     */
    public void main(String[] args) throws IOException, ParseException {
        loadEventList(); //Initially loads the events
        launch(args); //Launch the GUI
    }

    /**
     * Set up the stage, the components and defines the buttons which changes the scene
     * @param stage The stage on which everything happens
     */
    @Override
    public void start(Stage stage) {

        clsGUIManager.stage = stage;

        loadImages(); //Loads the images from the resource folder

        createPrimarySceneChildren(); //Creates everything which will be displayed at the primary scene
        createAboutSceneChildren(); //Creates everything which will be displayed at the about scene
        createSettingsSceneChildren(); //Creates everything which will be displayed at the settings scene

        stage.setTitle("Google Calendar GUI - Programmieren in Java"); //Set the title from the scene

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds(); //Get the bounds of the screen

        VBox primaryVBox = new VBox(); //VBox of the primary stage
        primaryVBox.getChildren().addAll(table, primaryGridPane); //Sets the components of the primary stage
        Scene primaryScene = new Scene(primaryVBox, primaryScreenBounds.getMaxX(), primaryScreenBounds.getMaxY()); //Puts the VBox into a scene

        Scene settingsScene = new Scene(settingsGridPane, primaryScreenBounds.getMaxX(), primaryScreenBounds.getMaxY()); //Put the settingsGridPane into a scene
        settingsScene.getStylesheets().add(getClass().getResource("guiStyle.css").toExternalForm()); //Add the css file to this scene

        VBox aboutVBox = new VBox(); //VBox of the about stage
        aboutVBox.getChildren().addAll(authorLabel, returnButtonAbout); //Sets the components of the primary stage
        Scene aboutScene = new Scene(aboutVBox, primaryScreenBounds.getMaxX(), primaryScreenBounds.getMaxY()); //Puts the VBox into a scene

        aboutButton.setOnAction(event -> stage.setScene(aboutScene)); //Defines what happens when you click the about button
        settingsButton.setOnAction(event -> stage.setScene(settingsScene)); //Defines what happens when you click the settings button
        returnButtonAbout.setOnAction(event -> stage.setScene(primaryScene)); //Defines what happens when you click the return button
        cancelButtonSettings.setOnAction(event -> stage.setScene(primaryScene)); //Defines what happens when you click the cancel button
        settingsSubmitButton.setOnAction(event -> { //Defines what happens when you click the submit button
            if (validateSettings()) { //Checks if the input is valid
                clsUserPreferences.setPreferences( //Sets the new user preferences
                        calendarURITextfield.textProperty().getValue(),
                        calendarExportTextfield.textProperty().getValue(),
                        startDateTextfield.textProperty().getValue(),
                        endDateTextfield.textProperty().getValue(),
                        searchTextfield.textProperty().getValue());
                try {
                    loadEventList(); //reload the event list with the new user preferences
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }
                table.getItems().clear(); //Clear the table
                table.getItems().addAll(events); //Put the new events in the table
                primaryVBox.getChildren().clear(); //Clears the components from the VBox
                primaryVBox.getChildren().addAll(table, primaryGridPane); //Add the new components the the VBox
                stage.setScene(primaryScene); //Set the stage in the primary scene
            }
        });

        stage.setScene(primaryScene); //Set the scene which will be seen when the GUI starts

        stage.setX(primaryScreenBounds.getMinX()); //Set the height from the screen
        stage.setY(primaryScreenBounds.getMinY()); //Set the height from the screen

        stage.show(); //Shows the stage
        stage.setMaximized(true); //Maximize the stage
    }

    /**
     * Creates everything which will be displayed at the primary scene
     */
    private void createPrimarySceneChildren() {

        table = new TableView<>(); //Creates the table
        table.prefHeightProperty().bind(stage.heightProperty()); //Bind the height property to the height from the stage

        TableColumn<Event, String> tableColumnName = new TableColumn<>("Title"); //Creates one Column with the name title
        tableColumnName.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getSummary())); //Defines which argument from Event should be displayed in this column
        tableColumnName.prefWidthProperty().bind(table.widthProperty().divide(3)); //Bind the width property to one third of the height of the table

        TableColumn<Event, String> tableColumnStart = new TableColumn<>("Start");
        tableColumnStart.setCellValueFactory((param) -> new SimpleStringProperty(parseDate(param.getValue().getStart())));
        tableColumnStart.prefWidthProperty().bind(table.widthProperty().divide(3));

        TableColumn<Event, String> tableColumnEnd = new TableColumn<>("End");
        tableColumnEnd.setCellValueFactory((param) -> new SimpleStringProperty(parseDate(param.getValue().getEnd())));
        tableColumnEnd.prefWidthProperty().bind(table.widthProperty().divide(3));

        table.getColumns().add(tableColumnName); //Add the columns
        table.getColumns().add(tableColumnStart);
        table.getColumns().add(tableColumnEnd);

        table.getItems().addAll(events); //Add the events to the table

        Button saveButton = new Button("File save", new ImageView(downloadImage)); //Creates the save button with image
        settingsButton = new Button("Settings", new ImageView(settingsImage)); //Creates the settings button with image
        aboutButton = new Button("About", new ImageView(aboutusImage)); //Creates the about button with image
        Button exitButton = new Button("Exit", new ImageView(exitImage)); //Creates the exit button with image

        primaryGridPane = new GridPane(); //Set up the GridPane with the buttons
        primaryGridPane.add(saveButton, 0, 0, 1, 1);
        primaryGridPane.add(settingsButton, 1, 0, 1, 1);
        primaryGridPane.add(aboutButton, 2, 0, 1, 1);
        primaryGridPane.add(exitButton, 3, 0, 1, 1);

        saveButton.setOnAction(event -> { //Defines the action when the save button is clicked
            try {
                clsComputeIcal.addItemsToIcsCalendar(events, clsUserPreferences.getPreferences().get(1)); //Creates the ical
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        exitButton.setOnAction(event -> System.exit(0)); //Defines the action when the exit button is clicked

    }

    /**
     * Creates everything which will be displayed at the about scene
     */
    private void createAboutSceneChildren() {
        authorLabel = new Label("Author: Tom Schmanke, integrated degree student of applied computer science at the Baden-Wuertemberg cooperative state university"); //Creates the text
        returnButtonAbout = new Button("Return", new ImageView(returnImage)); //Creates the return button with image
    }

    /**
     * Creates everything which will be displayed at the settings scene
     */
    private void createSettingsSceneChildren() {

        validationArray = new Boolean[6];
        Arrays.fill(validationArray, true); //Initializes the validation array with true in every place

        List<String> preferences = clsUserPreferences.getPreferences(); //Gets the userPreferences

        calendarURITextfield = new TextField(preferences.get(0)); //Fills the TextField with the value from the userPreferences
        calendarURITextfield.focusedProperty().addListener((arg0, oldValue, newValue) -> { //What happens when the value changes
            if (!newValue) { //False when the user stops focusing the field
                if (calendarURITextfield.getText() != null && !calendarURITextfield.getText().matches("\\w+")) { //Pattern matching for every word with at least one digit
                    validationArray[0] = false; //Sets the validation array at the place for this setting to false
                    calendarURITextfield.getStyleClass().remove("success"); //Removes the green border
                    calendarURITextfield.getStyleClass().add("error"); //Sets the red border
                } else {
                    validationArray[0] = true; //Sets the validation array at the place for this setting to true
                    calendarURITextfield.getStyleClass().remove("error"); //Removes the red border
                    calendarURITextfield.getStyleClass().add("success"); //Sets the green border
                }
            }
        });

        calendarExportTextfield = new TextField(preferences.get(1));
        calendarExportTextfield.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (calendarExportTextfield.getText() != null && !calendarExportTextfield.getText().matches("\\w+(\\.ics)")) { //Accepts anything which ends on .ics
                    validationArray[1] = false;
                    calendarExportTextfield.getStyleClass().remove("success");
                    calendarExportTextfield.getStyleClass().add("error");
                } else {
                    validationArray[1] = true;
                    calendarExportTextfield.getStyleClass().remove("error");
                    calendarExportTextfield.getStyleClass().add("success");
                }
            }
        });

        startDateTextfield = new TextField(preferences.get(2));
        startDateTextfield.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (startDateTextfield.getText() != null && !startDateTextfield.getText().matches("((3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4})|^$")) { //Accepts dd/MM/yyy
                    validationArray[2] = false;
                    startDateTextfield.getStyleClass().remove("success");
                    startDateTextfield.getStyleClass().add("error");
                } else {
                    validationArray[2] = true;
                    startDateTextfield.getStyleClass().remove("error");
                    startDateTextfield.getStyleClass().add("success");
                }
            }
        });

        endDateTextfield = new TextField(preferences.get(3));
        endDateTextfield.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (endDateTextfield.getText() != null && !endDateTextfield.getText().matches("(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}")) { //Accepts dd/MM/yyy
                    validationArray[3] = false;
                    endDateTextfield.getStyleClass().remove("success");
                    endDateTextfield.getStyleClass().add("error");
                } else {
                    validationArray[3] = true;
                    endDateTextfield.getStyleClass().remove("error");
                    endDateTextfield.getStyleClass().add("success");
                }
            }
        });

        searchTextfield = new TextField(preferences.get(4));
        searchTextfield.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (searchTextfield.getText() != null && !searchTextfield.getText().matches("(\\w*\\*)|(\\w*)")) { //Accepts anything with or without a * at the end
                    validationArray[4] = false;
                    searchTextfield.getStyleClass().remove("success");
                    searchTextfield.getStyleClass().add("error");
                } else {
                    validationArray[4] = true;
                    searchTextfield.getStyleClass().remove("error");
                    searchTextfield.getStyleClass().add("success");
                }
            }
        });

        settingsSubmitButton = new Button("Submit", new ImageView(checkmarkImage)); //Create the submit button with image
        cancelButtonSettings = new Button("Cancel", new ImageView(returnImage)); //Create the cancel button with image

        settingsGridPane = new GridPane(); //Set up GridPane with all the labels, TextFields and buttons
        settingsGridPane.add(new Label("Calender URI"), 0, 0, 1, 1);
        settingsGridPane.add(new Label("ICS-File Name"), 0, 1, 1, 1);
        settingsGridPane.add(new Label("Start date (dd/mm/yyyy)"), 0, 2, 1, 1);
        settingsGridPane.add(new Label("End date (dd/mm/yyyy)"), 0, 3, 1, 1);
        settingsGridPane.add(new Label("Search key"), 0, 4, 1, 1);
        settingsGridPane.add(calendarURITextfield, 1, 0, 2, 1);
        settingsGridPane.add(calendarExportTextfield, 1, 1, 2, 1);
        settingsGridPane.add(startDateTextfield, 1, 2, 2, 1);
        settingsGridPane.add(endDateTextfield, 1, 3, 2, 1);
        settingsGridPane.add(searchTextfield, 1, 4, 2, 1);
        settingsGridPane.add(settingsSubmitButton, 3, 0, 1, 1);
        settingsGridPane.add(cancelButtonSettings, 3, 1, 1, 1);

    }

    /**
     * Checks if the validationArray is true at every point
     * @return True if every entry is true, false if at least one if false
     */
    private boolean validateSettings() {
        for (Boolean aBoolean : validationArray) {
            if (!aBoolean) {
                return false;
            }
        }
        return true;
    }

    /**
     * Loads the googleEvents with the userPreferences
     * @throws IOException    See clsGetEvents.getEvents
     * @throws ParseException See clsGetEvents.getEvents
     */
    private void loadEventList() throws IOException, ParseException {
        List<String> userPreferences = clsUserPreferences.getPreferences(); //Gets the userPreferences
        events = clsGetEvents.getEvents(userPreferences.get(2), userPreferences.get(3), userPreferences.get(4));
    }

    /**
     * Loads the images from the resource folder
     */
    private void loadImages() {
        returnImage = new Image(getClass().getResourceAsStream("return.png"));
        aboutusImage = new Image(getClass().getResourceAsStream("aboutus.png"));
        checkmarkImage = new Image(getClass().getResourceAsStream("checkmark.png"));
        exitImage = new Image(getClass().getResourceAsStream("exit.png"));
        settingsImage = new Image(getClass().getResourceAsStream("settings.png"));
        downloadImage = new Image(getClass().getResourceAsStream("download.png"));
    }

    /**
     * Parses the EventDateTime into a String which can be displayed on the GUI
     * @param rawEventDateTime An EventDateTime from a googleEvent
     * @return A pretty String
     */
    private String parseDate(EventDateTime rawEventDateTime) {
        if (rawEventDateTime.getDate() != null) { //Checks whether it is a full day event or has a time
            return rawEventDateTime.getDate().toString().substring(0, 10); //Returns the important parts of the Date
        } else {
            String rawDate = rawEventDateTime.getDateTime().toString();
            return rawDate.substring(0, 10) + " " + rawDate.substring(11, 16); //Returns the important parts of the Date
        }
    }

}
