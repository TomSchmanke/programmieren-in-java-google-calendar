import com.google.api.services.calendar.model.Event;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * <p>
 * This class containing all necessary methods to get the information from the google API and convert them into an ics file
 */
public class clsComputeIcal {

    /**
     * Creates an empty calendar object with a few properties
     * @return The configured calendar
     */
    public net.fortuna.ical4j.model.Calendar createIcsCalendar() {
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0); //Sets the version of the calendar
        icsCalendar.getProperties().add(CalScale.GREGORIAN); //Sets the scale of the calender
        icsCalendar.getProperties().add(new ProdId("-//Ben Fortuna//iCal4j 1.0//EN")); //Sets the prod ID
        return icsCalendar;
    }

    /**
     * Read out the necessary information which are needed to create an VEvent
     * @param googleEvents A list of Events which should be in the the ics file
     * @param exportName   The name of the ics file
     * @throws IOException See getEvents
     */
    public void addItemsToIcsCalendar(List<Event> googleEvents, String exportName) throws IOException {

        net.fortuna.ical4j.model.Calendar icsCalendar = createIcsCalendar();

        for (Event googleEvent : googleEvents) {
            icsCalendar.getComponents().add(createEvent(googleEvent)); //Creates for every googleEvent a icsEvent and saves it in the icsCalendar
        }

        writeIcsFile(icsCalendar, exportName); //Writes the result into the ics file

    }

    /**
     * Turns a googleEvent into a icsEvent
     * @param googleEvent The googleEvent
     * @return The icsEvent
     */
    public VEvent createEvent(Event googleEvent) {

        VEvent icsEvent;

        if (googleEvent.getStart().getDate() != null) { //Checks if the event is full day or have start at a time
            icsEvent = new VEvent(new DateTime(googleEvent.getStart().getDate().getValue()), new DateTime(googleEvent.getEnd().getDate().getValue()), googleEvent.getSummary());
            //Creates the icsEvent with the title, start time and end time
        } else {
            icsEvent = new VEvent(new DateTime(googleEvent.getStart().getDateTime().getValue()), new DateTime(googleEvent.getEnd().getDateTime().getValue()), googleEvent.getSummary());
        }

        icsEvent.getProperties().add(new Description(googleEvent.getDescription())); //Add Description
        icsEvent.getProperties().add(new Uid(googleEvent.getICalUID())); //Add UID

        return icsEvent;
    }

    /**
     * Tries to create a ics file. If the file already exists it will be overridden. The data from the Google Calendar will be saved in the file.
     *
     * @param icsCalendar The data which will be written in the ics file
     * @param exportName  The name of the file in which the data will be stored
     * @throws IOException Gets thrown when the FileOutputStream can't get a file or can't write in it
     */
    public void writeIcsFile(net.fortuna.ical4j.model.Calendar icsCalendar, String exportName) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(exportName);
        CalendarOutputter outPutter = new CalendarOutputter();
        outPutter.output(icsCalendar, fileOutputStream); //Writes the date from the icsCalendar in the ics file
    }

}
