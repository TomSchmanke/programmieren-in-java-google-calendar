import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * <p>
 * Implementation of the Java Util Preferences API
 */
public class clsUserPreferences {

    private static final String CALENDER_URI = "calender_uri";
    private static final String ICS_NAME = "ics_name";
    private static final String START_DATE = "start_date";
    private static final String END_DATE = "end_date";
    private static final String SEARCH_KEY = "search_key";


    /**
     * Read out all the preferences and return it in a list of strings
     * @return A list of Strings containing the preferences
     */
    public List<String> getPreferences() {
        Preferences prefs = Preferences.userNodeForPackage(clsUserPreferences.class); //Get the saved preferences

        List<String> listOfPreferences = new ArrayList<>();
        listOfPreferences.add(prefs.get(CALENDER_URI, null)); //Get the value saved in the given preference with the standard value null
        listOfPreferences.add(prefs.get(ICS_NAME, "googleCalendar.ics"));
        listOfPreferences.add(prefs.get(START_DATE, null));
        listOfPreferences.add(prefs.get(END_DATE, null));
        listOfPreferences.add(prefs.get(SEARCH_KEY, null));

        return listOfPreferences;

    }

    /**
     * Sets all the preferences. If one preference is null, the saved value will be removed
     * @param calenderUriValue First Preference
     * @param icsNameValue Second Preference
     * @param startDateValue Third Preference
     * @param endDateValue Fourth Preference
     * @param searchKeyValue Fifth Preference
     */
    public void setPreferences(String calenderUriValue, String icsNameValue, String startDateValue, String endDateValue, String searchKeyValue) {
        Preferences prefs = Preferences.userNodeForPackage(clsUserPreferences.class);

        if (calenderUriValue == null) {
            prefs.remove(CALENDER_URI); //clears the value
        } else {
            prefs.put(CALENDER_URI, calenderUriValue); //sets the new value
        }

        if (icsNameValue == null) {
            prefs.remove(ICS_NAME);
        }else{
            prefs.put(ICS_NAME, icsNameValue);
        }

        if(startDateValue == null){
            prefs.remove(START_DATE);
        }else{
            prefs.put(START_DATE, startDateValue);
        }

        if(endDateValue == null){
            prefs.remove(END_DATE);
        }else{
            prefs.put(END_DATE, endDateValue);
        }

        if(searchKeyValue == null){
            prefs.remove(SEARCH_KEY);
        }else{
            prefs.put(SEARCH_KEY, searchKeyValue);
        }

    }
}
